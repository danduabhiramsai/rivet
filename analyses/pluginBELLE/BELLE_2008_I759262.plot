BEGIN PLOT /BELLE_2008_I759262/d01-x01-y01
Title=Cross section for $e^+e^-\to J/\psi X(3940)$ ($X(3940)\to D^*\bar{D}$)
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [fb]
END PLOT
BEGIN PLOT /BELLE_2008_I759262/d01-x01-y02
Title=Cross section for $e^+e^-\to J/\psi X(4160)$ ($X(4160)\to D^*\bar{D}^*$)
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [fb]
END PLOT
