BEGIN PLOT /BELLE_2020_I1785816/d01-x01-y01
Title=$K^-\pi^+$ mass distribution in $D^0\to K^-\pi^+\eta$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2020_I1785816/d01-x01-y02
Title=$\pi^+\eta$ mass distribution in $D^0\to K^-\pi^+\eta$
XLabel=$m^2_{\pi^+\eta}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\eta}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2020_I1785816/d01-x01-y03
Title=$K^-\eta$ mass distribution in $D^0\to K^-\pi^+\eta$
XLabel=$m^2_{K^-\eta}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\eta}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2020_I1785816/dalitz
Title=Dalitz plot for  $D^0\to K^-\pi^+\eta$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^+\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^-\pi^+}/{\rm d}m^2_{\pi^+\eta}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT