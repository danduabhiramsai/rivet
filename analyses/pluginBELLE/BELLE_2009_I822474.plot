BEGIN PLOT /BELLE_2009_I822474
LogY=0
YLabel=$\text{d}\sigma/\text{d}|\cos\theta|$ [nb]
XLabel=$|\cos\theta|$
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $|\cos\theta|<0.8$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \eta\pi^0)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $0.84<\sqrt{s}<0.86$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d03-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $0.86<\sqrt{s}<0.88$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d04-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $0.88<\sqrt{s}<0.90$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d05-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $0.90<\sqrt{s}<0.92$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d06-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $0.92<\sqrt{s}<0.94$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d07-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $0.94<\sqrt{s}<0.96$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d08-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $0.96<\sqrt{s}<0.98$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d09-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $0.98<\sqrt{s}<1.00$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d10-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.00<\sqrt{s}<1.02$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d11-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.02<\sqrt{s}<1.04$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d12-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.04<\sqrt{s}<1.06$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d13-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.06<\sqrt{s}<1.08$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d14-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.08<\sqrt{s}<1.10$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d15-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.10<\sqrt{s}<1.12$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d16-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.12<\sqrt{s}<1.14$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d17-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.14<\sqrt{s}<1.16$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d18-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.16<\sqrt{s}<1.18$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d19-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.18<\sqrt{s}<1.20$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d20-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.20<\sqrt{s}<1.22$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d21-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.22<\sqrt{s}<1.24$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d22-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.24<\sqrt{s}<1.26$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d23-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.26<\sqrt{s}<1.28$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d24-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.28<\sqrt{s}<1.30$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d25-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.30<\sqrt{s}<1.32$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d26-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.32<\sqrt{s}<1.34$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d27-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.34<\sqrt{s}<1.36$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d28-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.36<\sqrt{s}<1.38$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d29-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.38<\sqrt{s}<1.40$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d30-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.40<\sqrt{s}<1.42$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d31-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.42<\sqrt{s}<1.44$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d32-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.44<\sqrt{s}<1.46$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d33-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.46<\sqrt{s}<1.48$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d34-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.48<\sqrt{s}<1.50$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d35-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.50<\sqrt{s}<1.52$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d36-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.52<\sqrt{s}<1.54$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d37-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.54<\sqrt{s}<1.56$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d38-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.56<\sqrt{s}<1.58$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d39-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.58<\sqrt{s}<1.60$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d40-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.60<\sqrt{s}<1.64$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d41-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.64<\sqrt{s}<1.68$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d42-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.68<\sqrt{s}<1.72$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d43-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.72<\sqrt{s}<1.76$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d44-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.76<\sqrt{s}<1.80$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d45-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.80<\sqrt{s}<1.84$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d46-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.84<\sqrt{s}<1.88$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d47-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.88<\sqrt{s}<1.92$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d48-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.92<\sqrt{s}<1.96$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d49-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.96<\sqrt{s}<2.00$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d50-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.00<\sqrt{s}<2.04$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d51-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.04<\sqrt{s}<2.08$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d52-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.08<\sqrt{s}<2.12$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d53-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.12<\sqrt{s}<2.16$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d54-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.16<\sqrt{s}<2.20$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d55-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.20<\sqrt{s}<2.24$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d56-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.24<\sqrt{s}<2.28$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d57-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.28<\sqrt{s}<2.32$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d58-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.32<\sqrt{s}<2.36$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d59-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.36<\sqrt{s}<2.40$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d60-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.40<\sqrt{s}<2.50$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d61-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.50<\sqrt{s}<2.60$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d62-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.60<\sqrt{s}<2.70$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d63-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.70<\sqrt{s}<2.80$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d64-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.80<\sqrt{s}<2.90$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d65-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $2.90<\sqrt{s}<3.00$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d66-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $3.00<\sqrt{s}<3.10$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d67-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $3.10<\sqrt{s}<3.20$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d68-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $3.20<\sqrt{s}<3.30$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d69-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $3.30<\sqrt{s}<3.40$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d70-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $3.40<\sqrt{s}<3.50$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d71-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $3.50<\sqrt{s}<3.60$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d72-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $3.60<\sqrt{s}<3.70$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d73-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $3.70<\sqrt{s}<3.80$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d74-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $3.80<\sqrt{s}<3.90$ GeV
END PLOT
BEGIN PLOT /BELLE_2009_I822474/d75-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $3.90<\sqrt{s}<4.00$ GeV
END PLOT
