BEGIN PLOT /BELLE_2015_I1326905/d01-x01-y01
Title=$D_s^-K^0_S$ mass in $B^0\to D^-_sK^0_S\pi^+$
XLabel=$m_{D_s^-K^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D_s^-K^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2015_I1326905/d01-x01-y02
Title=$D_s^-K^+$ mass in $B^0\to D^-_sK^+K^+$
XLabel=$m_{D_s^-K^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D_s^-K^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
