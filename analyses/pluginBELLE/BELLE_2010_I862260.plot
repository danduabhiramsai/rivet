BEGIN PLOT /BELLE_2010_I862260
LogY=0
YLabel=$\text{d}\sigma/\text{d}|\cos\theta|$ [nb]
XLabel=$|\cos\theta|$
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $|\cos\theta|<0.9$
XLabel=$\sqrt{s}$
YLabel=$\sigma(\gamma\gamma\to \eta\eta)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d01-x01-y02
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $|\cos\theta|<1.0$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \eta\eta)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.096<\sqrt{s}<1.12$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d03-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.12<\sqrt{s}<1.16$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d04-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.16<\sqrt{s}<1.20$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d05-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.20<\sqrt{s}<1.24$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d06-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.24<\sqrt{s}<1.28$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d07-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.28<\sqrt{s}<1.32$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d08-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.32<\sqrt{s}<1.36$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d09-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.36<\sqrt{s}<1.40$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d10-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.40<\sqrt{s}<1.44$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d11-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.44<\sqrt{s}<1.48$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d12-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.48<\sqrt{s}<1.52$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d13-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.52<\sqrt{s}<1.56$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d14-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.56<\sqrt{s}<1.60$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d15-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.60<\sqrt{s}<1.64$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d16-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.64<\sqrt{s}<1.68$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d17-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.68<\sqrt{s}<1.72$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d18-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.72<\sqrt{s}<1.76$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d19-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.76<\sqrt{s}<1.80$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d20-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.80<\sqrt{s}<1.84$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d21-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.84<\sqrt{s}<1.88$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d22-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.88<\sqrt{s}<1.92$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d23-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.22<\sqrt{s}<1.96$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d24-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $1.96<\sqrt{s}<2.00$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d25-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.00<\sqrt{s}<2.04$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d26-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.04<\sqrt{s}<2.08$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d27-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.08<\sqrt{s}<2.12$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d28-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.12<\sqrt{s}<2.16$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d29-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.16<\sqrt{s}<2.20$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d30-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.20<\sqrt{s}<2.24$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d31-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.24<\sqrt{s}<2.28$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d32-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.28<\sqrt{s}<2.32$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d33-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.32<\sqrt{s}<2.36$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d34-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.36<\sqrt{s}<2.40$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d35-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.40<\sqrt{s}<2.50$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d36-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.50<\sqrt{s}<2.60$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d37-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.60<\sqrt{s}<2.70$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d38-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.70<\sqrt{s}<2.80$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d39-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.80<\sqrt{s}<2.90$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d40-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $2.90<\sqrt{s}<3.00$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d41-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $3.00<\sqrt{s}<3.10$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d42-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $3.10<\sqrt{s}<3.20$ GeV
END PLOT
BEGIN PLOT /BELLE_2010_I862260/d43-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\eta$ with $3.20<\sqrt{s}<3.30$ GeV
END PLOT
