BEGIN PLOT /BELLE_2009_I816632/d01-x01-y01
Title=$D_s^-K^+$ mass in $B^+\to D^-_sK^+\pi^-$
XLabel=$m_{D_s^-K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{D_s^-K^+}$ [$\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2009_I816632/d01-x01-y02
Title=$D_s^{*-}K^+$ mass in $B^+\to D^{*-}_sK^+\pi^-$
XLabel=$m_{D_s^{*-}K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{D_s^{*-}K^+}$ [$\mathrm{GeV}$]
LogY=0
END PLOT
