BEGIN PLOT /BELLE_2009_I811289
LogY=0
END PLOT
BEGIN PLOT /BELLE_2009_I811289/d01-x01-y01
Title=Cross Section for $e^+e^-\to J\psi c\bar{c}$
XLabel=$sqrt{s}$
YLabel=$\sigma$ [pb]
END PLOT
BEGIN PLOT /BELLE_2009_I811289/d01-x01-y01
Title=Cross Section for $e^+e^-\to J\psi X_{\text{non}-c\bar{c}}$
XLabel=$sqrt{s}$
YLabel=$\sigma$ [pb]
END PLOT

BEGIN PLOT /BELLE_2009_I811289/d02
XLabel=$p_{J/\psi}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{J/\psi}$ [$\text{fb}/\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2009_I811289/d02-x01-y01
Title=Differential cross w.r.t $p_{J/\psi}$ for inclusive $J/\psi$ production
END PLOT
BEGIN PLOT /BELLE_2009_I811289/d02-x01-y02
Title=Differential cross w.r.t $p_{J/\psi}$ for $J/\psi$ production with a charm meson
END PLOT
BEGIN PLOT /BELLE_2009_I811289/d02-x01-y03
Title=Differential cross w.r.t $p_{J/\psi}$ for $J/\psi$ production in double charmonium production
END PLOT
BEGIN PLOT /BELLE_2009_I811289/d02-x01-y04
Title=Differential cross w.r.t $p_{J/\psi}$ for $J/\psi$ production with a $c\bar{c}$ pair
END PLOT
BEGIN PLOT /BELLE_2009_I811289/d02-x01-y05
Title=Differential cross w.r.t $p_{J/\psi}$ for inclusive $J/\psi$ without a $c\bar{c}$ pair
END PLOT

BEGIN PLOT /BELLE_2009_I811289/d03-x01
XLabel=$\cos\theta_{\text{hel}}$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_{\text{hel}}$
END PLOT
BEGIN PLOT /BELLE_2009_I811289/d03-x01-y01
Title=$J/\psi$ production angle (inclusive)
END PLOT
BEGIN PLOT /BELLE_2009_I811289/d03-x01-y02
Title=$J/\psi$ production angle (with $c\bar{c}$ pair)
END PLOT
BEGIN PLOT /BELLE_2009_I811289/d03-x01-y03
Title=$J/\psi$ production angle (without a $c\bar{c}$ pair)
END PLOT

BEGIN PLOT /BELLE_2009_I811289/d03-x02
XLabel=$\cos\theta_{\text{hel}}$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_{\text{prod}}$
END PLOT
BEGIN PLOT /BELLE_2009_I811289/d03-x02-y01
Title=$J/\psi$ helicity angle (inclusive)
END PLOT
BEGIN PLOT /BELLE_2009_I811289/d03-x02-y02
Title=$J/\psi$ helicity angle (with $c\bar{c}$ pair)
END PLOT
BEGIN PLOT /BELLE_2009_I811289/d03-x02-y03
Title=$J/\psi$ helicity angle (without a $c\bar{c}$ pair)
END PLOT
