BEGIN PLOT /BELLE_2021_I1917200
LogY=0
XLabel=$q^2_{\min}$ [$\mathrm{GeV}^2$]
END PLOT

BEGIN PLOT /BELLE_2021_I1917200/d01-x01-y01
Title=$\langle q^2\rangle$ moment in $B\to X_ce\nu_e$
YLabel=$\langle q^2\rangle$ [$\mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /BELLE_2021_I1917200/d02-x01-y01
Title=$\langle q^4\rangle$ moment in $B\to X_ce\nu_e$
YLabel=$\langle q^4\rangle$ [$\mathrm{GeV}^4$]
END PLOT
BEGIN PLOT /BELLE_2021_I1917200/d03-x01-y01
Title=$\langle q^6\rangle$ moment in $B\to X_ce\nu_e$
YLabel=$\langle q^6\rangle$ [$\mathrm{GeV}^6$]
END PLOT
BEGIN PLOT /BELLE_2021_I1917200/d04-x01-y01
Title=$\langle q^8\rangle$ moment in $B\to X_ce\nu_e$
YLabel=$\langle q^8\rangle$ [$\mathrm{GeV}^8$]
END PLOT
BEGIN PLOT /BELLE_2021_I1917200/d09-x01-y01
Title=$\langle\left(q^2-\langle q^2\rangle\right)^2\rangle$ moment in $B\to X_ce\nu_e$
YLabel=$\langle\left(q^2-\langle q^2\rangle\right)^2\rangle$ [$\mathrm{GeV}^4$]
END PLOT
BEGIN PLOT /BELLE_2021_I1917200/d10-x01-y01
Title=$\langle\left(q^2-\langle q^2\rangle\right)^3\rangle$ moment in $B\to X_ce\nu_e$
YLabel=$\langle\left(q^2-\langle q^2\rangle\right)^3\rangle$ [$\mathrm{GeV}^6$]
END PLOT
BEGIN PLOT /BELLE_2021_I1917200/d11-x01-y01
Title=$\langle\left(q^2-\langle q^2\rangle\right)^4\rangle$ moment in $B\to X_ce\nu_e$
YLabel=$\langle\left(q^2-\langle q^2\rangle\right)^4\rangle$ [$\mathrm{GeV}^8$]
END PLOT

BEGIN PLOT /BELLE_2021_I1917200/d05-x01-y01
Title=$\langle q^2\rangle$ moment in $B\to X_c\mu\nu_\mu$
YLabel=$\langle q^2\rangle$ [$\mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /BELLE_2021_I1917200/d06-x01-y01
Title=$\langle q^4\rangle$ moment in $B\to X_c\mu\nu_\mu$
YLabel=$\langle q^4\rangle$ [$\mathrm{GeV}^4$]
END PLOT
BEGIN PLOT /BELLE_2021_I1917200/d07-x01-y01
Title=$\langle q^6\rangle$ moment in $B\to X_c\mu\nu_\mu$
YLabel=$\langle q^6\rangle$ [$\mathrm{GeV}^6$]
END PLOT
BEGIN PLOT /BELLE_2021_I1917200/d08-x01-y01
Title=$\langle q^8\rangle$ moment in $B\to X_c\mu\nu_\mu$
YLabel=$\langle q^8\rangle$ [$\mathrm{GeV}^8$]
END PLOT
BEGIN PLOT /BELLE_2021_I1917200/d12-x01-y01
Title=$\langle\left(q^2-\langle q^2\rangle\right)^2\rangle$ moment in $B\to X_c\mu\nu_\mu$
YLabel=$\langle\left(q^2-\langle q^2\rangle\right)^2\rangle$ [$\mathrm{GeV}^4$]
END PLOT
BEGIN PLOT /BELLE_2021_I1917200/d13-x01-y01
Title=$\langle\left(q^2-\langle q^2\rangle\right)^3\rangle$ moment in $B\to X_c\mu\nu_\mu$
YLabel=$\langle\left(q^2-\langle q^2\rangle\right)^3\rangle$ [$\mathrm{GeV}^6$]
END PLOT
BEGIN PLOT /BELLE_2021_I1917200/d14-x01-y01
Title=$\langle\left(q^2-\langle q^2\rangle\right)^4\rangle$ moment in $B\to X_c\mu\nu_\mu$
YLabel=$\langle\left(q^2-\langle q^2\rangle\right)^4\rangle$ [$\mathrm{GeV}^8$]
END PLOT
