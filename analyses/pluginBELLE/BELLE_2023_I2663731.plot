BEGIN PLOT /BELLE_2023_I2663731/d01-x01-y01
Title=Differentital branching ratio w.r.t $m_{p\bar\Lambda^0}$ for $B^0\to p\bar\Lambda^0\pi^-$
XLabel=$m_{p\bar\Lambda^0}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{p\bar\Lambda^0}$ [$10^{-6}/\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2023_I2663731/d01-x01-y02
Title=Differentital branching ratio w.r.t $m_{p\bar\Sigma^0}$ for $B^0\to p\bar\Sigma^0\pi^-$
XLabel=$m_{p\bar\Sigma^0}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{p\bar\Sigma^0}$ [$10^{-6}/\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2023_I2663731/d02-x01-y01
Title=Differentital Br w.r.t $\cos\theta_p$ for $B^0\to p\bar\Lambda^0\pi^-$ ($m_{p\bar\Lambda^0}<2.8$GeV)
XLabel=$\cos\theta_p$ 
YLabel=$\text{d}\mathcal{B}/\text{d}\cos\theta_p$ [$10^{-6}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2023_I2663731/d02-x01-y02
Title=Differentital Br w.r.t $\cos\theta_p$ for $B^0\to p\bar\Sigma^0\pi^-$ ($m_{p\bar\Sigma^0}<2.8$GeV)
XLabel=$\cos\theta_p$ 
YLabel=$\text{d}\mathcal{B}/\text{d}\cos\theta_p$ [$10^{-6}$]
LogY=0
END PLOT
