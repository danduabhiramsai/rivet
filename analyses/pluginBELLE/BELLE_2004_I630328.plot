BEGIN PLOT /BELLE_2004_I630328/d01
XLabel=$m_{p\bar{p}}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{p\bar{p}}\times10^6$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I630328/d01-x01-y01
Title=$p\bar{p}$ mass for $B^+\to p\bar{p}K^+$
END PLOT
BEGIN PLOT /BELLE_2004_I630328/d01-x01-y02
Title=$p\bar{p}$ mass for $B^+\to p\bar{p}\pi^+$
END PLOT
BEGIN PLOT /BELLE_2004_I630328/d01-x01-y03
Title=$p\bar{p}$ mass for $B^0\to p\bar{p}K^0_S$
END PLOT
BEGIN PLOT /BELLE_2004_I630328/d01-x01-y04
Title=$p\bar{p}$ mass for $B^+\to p\bar{p}K^{*+}$
END PLOT
