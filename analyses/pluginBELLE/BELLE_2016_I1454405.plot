BEGIN PLOT /BELLE_2016_I1454405/d01-x01-y01
Title=Spectrum of $J/\psi$ in $\Upsilon(1S)$ decays
XLabel=$x_p$
YLabel=$\mathrm{d}\mathcal{B}/\mathrm{d}x_p\times10^4$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2016_I1454405/d01-x02-y01
Title=Branching ratio for $J/\psi+X$ in $\Upsilon(1S)$ decays
XLabel=$x_p$
YLabel=$\mathcal{B}\times10^4$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2016_I1454405/d01-x01-y02
Title=Spectrum of $\psi(2S)$ in $\Upsilon(1S)$ decays
XLabel=$x_p$
YLabel=$\mathrm{d}\mathcal{B}/\mathrm{d}x_p\times10^4$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2016_I1454405/d01-x02-y02
Title=Branching ratio for $\psi(2S)+X$ in $\Upsilon(1S)$ decays
XLabel=$x_p$
YLabel=$\mathcal{B}\times10^4$
LogY=0
END PLOT
