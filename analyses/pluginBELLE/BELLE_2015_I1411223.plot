BEGIN PLOT /BELLE_2015_I1411223/d01-x01-y01
Title=Cross Section for $(BB^*)^\pm\pi^\mp$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2015_I1411223/d01-x01-y02
Title=Cross Section for $(B^*B^*)^\pm\pi^\mp$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$
LogY=0
END PLOT
