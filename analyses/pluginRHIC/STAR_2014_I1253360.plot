BEGIN PLOT /STAR_2014_I1253360/d08-x01-y01
Title=$\pi^0$ cross section, $\sqrt{s} = 200$ GeV, $0.8 < \eta < 2.0$
XLabel=$p_T$ (GeV$/c$)
YLabel=$E \frac{\mathrm{d}^3 \sigma}{\mathrm{d} p^3}$ ($\text{mb}\:\text{GeV}^{-2} c^3$)
END PLOT
