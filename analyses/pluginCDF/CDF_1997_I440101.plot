BEGIN PLOT /CDF_1997_I440101/d01-x01-y01
Title=$J/\psi$ cross section ($p_\perp>5$\,GeV, $|\eta|<0.6$)
XLabel=
YLabel=$\sigma\times\text{Br}(J/\psi\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /CDF_1997_I440101/d01-x01-y02
Title=$\psi(2S)$ cross section ($p_\perp>5$\,GeV, $|\eta|<0.6$)
XLabel=
YLabel=$\sigma\times\text{Br}(\psi(2S)\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /CDF_1997_I440101/d02-x01-y01
Title=Non-prompt $J/\psi$ cross section ($|\eta|<0.6$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp\times\text{Br}(J/\psi\to\mu^+\mu^-)$ [nb/GeV]
END PLOT
BEGIN PLOT /CDF_1997_I440101/d03-x01-y01
Title=Non-prompt $\psi(2S)$ cross section ($|\eta|<0.6$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp\times\text{Br}(\psi(2S)\to\mu^+\mu^-)$ [nb/GeV]
END PLOT
BEGIN PLOT /CDF_1997_I440101/d04-x01-y01
Title=Prompt $J/\psi$ cross section ($|\eta|<0.6$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp\times\text{Br}(J/\psi\to\mu^+\mu^-)$ [nb/GeV]
END PLOT
BEGIN PLOT /CDF_1997_I440101/d05-x01-y01
Title=Prompt $\psi(2S)$ cross section ($|\eta|<0.6$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp\times\text{Br}(\psi(2S)\to\mu^+\mu^-)$ [nb/GeV]
END PLOT
