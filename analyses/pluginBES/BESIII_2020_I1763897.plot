BEGIN PLOT /BESIII_2020_I1763897/d01-x01-y01
Title=$\phi\phi$ mass distribution in $\chi_{c0}\to \phi\phi\eta$
XLabel=$m_{\phi\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\phi\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1763897/d01-x01-y02
Title=$\phi\eta$ mass distribution in $\chi_{c0}\to \phi\phi\eta$
XLabel=$m_{\phi\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\phi\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1763897/dalitz_1
Title=Dalitz plot for $\chi_{c0}\to \phi\phi\eta$
XLabel=$m^2_{\phi\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\phi\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\phi\eta}/{\rm d}m^2_{\phi\eta}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1763897/d02-x01-y01
Title=$\phi\phi$ mass distribution in $\chi_{c1}\to \phi\phi\eta$
XLabel=$m_{\phi\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\phi\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1763897/d02-x01-y02
Title=$\phi\eta$ mass distribution in $\chi_{c1}\to \phi\phi\eta$
XLabel=$m_{\phi\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\phi\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1763897/dalitz_2
Title=Dalitz plot for $\chi_{c1}\to \phi\phi\eta$
XLabel=$m^2_{\phi\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\phi\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\phi\eta}/{\rm d}m^2_{\phi\eta}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1763897/d03-x01-y01
Title=$\phi\phi$ mass distribution in $\chi_{c2}\to \phi\phi\eta$
XLabel=$m_{\phi\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\phi\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1763897/d03-x01-y02
Title=$\phi\eta$ mass distribution in $\chi_{c2}\to \phi\phi\eta$
XLabel=$m_{\phi\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\phi\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1763897/dalitz_3
Title=Dalitz plot for $\chi_{c2}\to \phi\phi\eta$
XLabel=$m^2_{\phi\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\phi\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\phi\eta}/{\rm d}m^2_{\phi\eta}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
