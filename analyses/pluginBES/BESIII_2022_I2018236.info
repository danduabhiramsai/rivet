Name: BESIII_2022_I2018236
Year: 2022
Summary: Radiative $J/\psi\to\gamma \eta^\prime\pi^+\pi^-$ decay
Experiment: BESIII
Collider: BEPC
InspireID: 2018236
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 129 (2022) 4, 042001
RunInfo: Any process producing J/psi originally e+e-
Description:
  'Measurement of the hadronic mass distribution in the decay $J/\psi\to\gamma \eta^\prime\pi^+\pi^-$. The data were read from the plots in the paper. It is also not clear that any resolution effects have been unfolded.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022iiq
BibTeX: '@article{BESIII:2022iiq,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Observation of a State $X(2600)$ in the $\pi^{+}\pi^{-}\eta^\prime$ System in the Process $J/\psi\rightarrow\gamma\pi^{+}\pi^{-}\eta^\prime$}",
    eprint = "2201.10796",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevLett.129.042001",
    journal = "Phys. Rev. Lett.",
    volume = "129",
    number = "4",
    pages = "042001",
    year = "2022"
}
'
