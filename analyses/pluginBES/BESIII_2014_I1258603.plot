BEGIN PLOT /BESIII_2014_I1258603/d01-x01-y01
Title=Cross section for $e^+e^-\to \gamma X(3872)(\to\pi^+\pi^- J/\psi)$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \gamma X(3872)\times\mathcal{B}( X(3872) \to\pi^+\pi^- J/\psi)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
