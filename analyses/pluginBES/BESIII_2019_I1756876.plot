BEGIN PLOT /BESIII_2019_I1756876/d01-x01-y01
Title=Cross section for $e^+e^-\to D^\pm_1(2420)D^\mp\to D^+D^-\pi^+\pi^-$
XLabel=$E_{\mathrm{CMS}}$ [MeV]
YLabel=$\sigma(e^+e^-\to D^\pm_1(2420)D^\mp\to D^+D^-\pi^+\pi^-)/pb$
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2019_I1756876/d01-x01-y02
Title=Cross section for $e^+e^-\to \psi(3770)\pi^+\pi^-\to D^+D^-\pi^+\pi^-$
XLabel=$E_{\mathrm{CMS}}$ [MeV]
YLabel=$\sigma(e^+e^-\to \psi(3770)\pi^+\pi^-\to D^+D^-\pi^+\pi^-)/pb$
LogY=0
ConnectGaps=1
END PLOT
