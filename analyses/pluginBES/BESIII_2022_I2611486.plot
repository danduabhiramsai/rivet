BEGIN PLOT /BESIII_2022_I2611486/d01-x01-y01
Title=Cross section for $e^+e^-\to\Omega^-\bar{\Omega}^+$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\Omega^-\bar{\Omega}^+)$ [fb]
ConnectGaps=1
LogY=0
END PLOT
