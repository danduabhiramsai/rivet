BEGIN PLOT /BESII_2007_I763951/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $\psi(2S)\to\gamma\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2007_I763951/d02-x01-y01
Title=$K^+K^-$ mass distribution in $\psi(2S)\to\gamma K^+K^-$
XLabel=$m_{K^+K^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
