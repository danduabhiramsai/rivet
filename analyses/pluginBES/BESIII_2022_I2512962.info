Name: BESIII_2022_I2512962
Year: 2022
Summary: Cross section for $e^+e^-\to\eta\Lambda\bar{\Lambda}$ from 3.51 to 4.7 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 2512962
Status: VALIDATED NOHEPDATA 
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2211.10755 [hep-ex]
RunInfo: e+ e- to hadrons
Beams: [e+, e-]
Energies : [3.5106, 3.773, 3.872, 4.0076, 4.1285, 4.1574, 4.1784, 4.1888, 4.1989, 4.2092, 4.2187, 4.2263,
            4.2357, 4.2438, 4.258, 4.2668, 4.2879, 4.3121, 4.3374, 4.3583, 4.3774, 4.3964, 4.4156, 4.44,
            4.4671, 4.5995, 4.628, 4.6612, 4.6409, 4.6819, 4.6988]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to\eta\Lambda\bar{\Lambda}$ from 3.51 to 4.7 GeV by the BESIII collaboration.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022tvj
BibTeX: '@article{BESIII:2022tvj,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Measurement of $e^+e^-\rightarrow\Lambda\bar{\Lambda}\eta$ from 3.5106 to 4.6988 GeV and study of $\Lambda\bar{\Lambda}$ mass threshold enhancement}",
    eprint = "2211.10755",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "11",
    year = "2022"
}
'
