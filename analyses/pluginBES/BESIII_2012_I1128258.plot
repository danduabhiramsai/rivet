BEGIN PLOT /BESIII_2012_I1128258/d01-x01-y01
Title=$p\pi^-$ mass distribution in $\chi_{c0}\to p\bar{n}\pi^-$
XLabel=$m_{p\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2012_I1128258/d01-x01-y02
Title=$\bar{n}\pi^-$ mass distribution in $\chi_{c0}\to p\bar{n}\pi^-$
XLabel=$m_{\bar{n}\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{n}\pi^-t}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2012_I1128258/d01-x01-y03
Title=$p\bar{n}$ mass distribution in $\chi_{c0}\to p\bar{n}\pi^-$
XLabel=$m_{p\bar{n}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar{n}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2012_I1128258/d02-x01-y01
Title=$\pi^-\pi^0$ mass distribution in $\chi_{cJ}\to p\bar{n}\pi^-\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
