BEGIN PLOT /BESIII_2023_I2689064
LogY=0
XLabel=$q^2$ [$\text{GeV}^2$]
END PLOT

BEGIN PLOT /BESIII_2023_I2689064/d01-x01-y01
Title=Differential decay rate vs $q^2$ for $\Lambda_c^+\to\Lambda^0 e^+\nu_e$
YLabel=$\text{d}\Gamma/\text{d}q^2$ [$\text{ps}^{-1}\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d01-x02-y01
Title=Differential decay rate vs $q^2$ for $\Lambda_c^+\to\Lambda^0 \mu^+\nu_\mu$
YLabel=$\text{d}\Gamma/\text{d}q^2$ [$\text{ps}^{-1}\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d01-x03-y01
Title=Ratio of $\mu/e$ differential decay rates
YLabel=$R^{\mu/e}_{\text{d}\Gamma/\text{d}q^2}$
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d01-x01-y02
Title=Leptonic forward-backward asymmetry for $\Lambda_c^+\to\Lambda^0 e^+\nu_e$
YLabel=$A^\ell_{\text{FB}}$
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d01-x02-y02
Title=Leptonic forward-backward asymmetry for $\Lambda_c^+\to\Lambda^0 \mu^+\nu_\mu$
YLabel=$A^\ell_{\text{FB}}$
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d01-x03-y02
Title=Ratio of $\mu/e$ Leptonic forward-backward asymmetry
YLabel=$R^{\mu/e}_{A^\ell_{\text{FB}}}$
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d02-x01-y01
Title=Proton forward-backward asymmetry for $\Lambda_c^+\to\Lambda^0 e^+\nu_e$
YLabel=$A^p_{\text{FB}}$
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d02-x01-y02
Title=Proton forward-backward asymmetry for $\Lambda_c^+\to\Lambda^0 \mu^+\nu_\mu$
YLabel=$A^p_{\text{FB}}$
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d02-x02-y01
Title=$\alpha_{\Lambda_c}$ for $\Lambda_c^+\to\Lambda^0 \ell^+\nu_\ell$
YLabel=$\alpha_{\Lambda_c}$
END PLOT

BEGIN PLOT /BESIII_2023_I2689064/d03-x01-y01
Title=$q^2$ distribution for $\Lambda_c^+\to\Lambda^0 e^+\nu_e$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}q^2$  [$\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d03-x01-y02
Title=$\cos\theta_p$ distribution for $\Lambda_c^+\to\Lambda^0 e^+\nu_e$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_p$
XLabel=$\cos\theta_p$
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d03-x01-y03
Title=$\cos\theta_e$ distribution for $\Lambda_c^+\to\Lambda^0 e^+\nu_e$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_e$
XLabel=$\cos\theta_e$
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d03-x01-y04
Title=$\chi$ distribution for $\Lambda_c^+\to\Lambda^0 e^+\nu_e$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\chi$ [$\text{rad}^{-1}$]
XLabel=$\chi$ [rad]
END PLOT

BEGIN PLOT /BESIII_2023_I2689064/d03-x02-y01
Title=$q^2$ distribution for $\Lambda_c^+\to\Lambda^0 \mu^+\nu_\mu$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}q^2$  [$\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d03-x02-y02
Title=$\cos\theta_p$ distribution for $\Lambda_c^+\to\Lambda^0 \mu^+\nu_\mu$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_p$
XLabel=$\cos\theta_p$
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d03-x02-y03
Title=$\cos\theta_\mu$ distribution for $\Lambda_c^+\to\Lambda^0 \mu^+\nu_\mu$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\mu$
XLabel=$\cos\theta_\mu$
END PLOT
BEGIN PLOT /BESIII_2023_I2689064/d03-x02-y04
Title=$\chi$ distribution for $\Lambda_c^+\to\Lambda^0 \mu^+\nu_\mu$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\chi$ [$\text{rad}^{-1}$]
XLabel=$\chi$ [rad]
END PLOT
