BEGIN PLOT /BES_2005_I689969/d01-x01-y01
Title=$\pi\pi$ mass distribution in $\psi(2S)\to \pi^+\pi^-\pi^0$
XLabel=$m_{\pi\pi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi\pi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BES_2005_I689969/dalitz
Title=Dalitz plot for $\psi(2S)\to \pi^+\pi^-\pi^0$
XLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^-\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^+\pi^0}/{\rm d}m^2_{\pi^-\pi^0}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT