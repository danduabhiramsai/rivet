BEGIN PLOT /BES_1999_I498114/d01-x01-y01
Title=$\eta\pi^+\pi^-$ mass distribution in $J/\psi\to\gamma \eta\pi^+\pi^-$
XLabel=$m_{\eta\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\eta\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
