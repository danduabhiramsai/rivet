BEGIN PLOT /BESIII_2017_I1507887
LogY=0
END PLOT

BEGIN PLOT /BESIII_2017_I1507887/d01-x01-y01
Title=$\cos\theta_1$ for $\gamma_1$ in $\psi(2S)\to\gamma_1\chi_{c1}$
XLabel=$\cos\theta_1$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_1$
END PLOT
BEGIN PLOT /BESIII_2017_I1507887/d01-x01-y02
Title=$\cos\theta_2$ for $\gamma_2$ in $\psi(2S)\to\gamma_1\chi_{c1}$, $\chi_{c1}\to\gamma_2J/\psi$
XLabel=$\cos\theta_2$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_2$
END PLOT
BEGIN PLOT /BESIII_2017_I1507887/d01-x01-y03
Title=$\cos\theta_3$ for $\ell^+$ in $\psi(2S)\to\gamma_1\chi_{c1}$, $\chi_{c1}\to\gamma_2J/\psi$, $J\psi\to\ell^+\ell^-$
XLabel=$\cos\theta_3$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_3$
END PLOT
BEGIN PLOT /BESIII_2017_I1507887/d01-x01-y04
Title=$\phi_2$ for $\gamma_2$ in $\psi(2S)\to\gamma_1\chi_{c1}$, $\chi_{c1}\to\gamma_2J/\psi$
XLabel=$\phi_2$ [rad]
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\phi_2$ [$\mathrm{rad}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2017_I1507887/d01-x01-y05
Title=$\phi_3$ for $\ell^+$ in $\psi(2S)\to\gamma_1\chi_{c1}$, $\chi_{c1}\to\gamma_2J/\psi$, $J\psi\to\ell^+\ell^-$
XLabel=$\phi_3$ [rad]
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\phi_3$ [$\mathrm{rad}^{-1}$]
END PLOT

BEGIN PLOT /BESIII_2017_I1507887/d01-x01-y06
Title=$\cos\theta_1$ for $\gamma_1$ in $\psi(2S)\to\gamma_1\chi_{c2}$
XLabel=$\cos\theta_1$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_1$
END PLOT
BEGIN PLOT /BESIII_2017_I1507887/d01-x01-y07
Title=$\cos\theta_2$ for $\gamma_2$ in $\psi(2S)\to\gamma_1\chi_{c2}$, $\chi_{c2}\to\gamma_2J/\psi$
XLabel=$\cos\theta_2$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_2$
END PLOT
BEGIN PLOT /BESIII_2017_I1507887/d01-x01-y08
Title=$\cos\theta_3$ for $\ell^+$ in $\psi(2S)\to\gamma_1\chi_{c2}$, $\chi_{c2}\to\gamma_2J/\psi$, $J\psi\to\ell^+\ell^-$
XLabel=$\cos\theta_3$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_3$
END PLOT
BEGIN PLOT /BESIII_2017_I1507887/d01-x01-y09
Title=$\phi_2$ for $\gamma_2$ in $\psi(2S)\to\gamma_1\chi_{c2}$, $\chi_{c2}\to\gamma_2J/\psi$
XLabel=$\phi_2$ [rad]
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\phi_2$ [$\mathrm{rad}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2017_I1507887/d01-x01-y10
Title=$\phi_3$ for $\ell^+$ in $\psi(2S)\to\gamma_1\chi_{c2}$, $\chi_{c2}\to\gamma_2J/\psi$, $J\psi\to\ell^+\ell^-$
XLabel=$\phi_3$ [rad]
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\phi_3$ [$\mathrm{rad}^{-1}$]
END PLOT
