BEGIN PLOT /BES_2001_I556330/d01-x01-y01
Title=$p(\bar{p})\eta$ mass distribution in $J/\psi\to p\bar{p}\eta$
XLabel=$m_{p\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p(\bar{p})\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BES_2001_I556330/d02-x01-y01
Title=$p\bar{p}$ mass distribution in $J/\psi\to p\bar{p}\eta$
XLabel=$m_{p\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BES_2001_I556330/dalitz
Title=Dalitz plot for  $J/\psi\to p\bar{p}\eta$
XLabel=$m^2_{p\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{p}\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{p\eta}/{\rm d}m^2_{\bar{p}\eta}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
