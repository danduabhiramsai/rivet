BEGIN PLOT /BESIII_2017_I1600581/d01-x01-y01
Title=Cross section for $e^+e^-\to\gamma \eta_c$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\gamma \eta_c)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
