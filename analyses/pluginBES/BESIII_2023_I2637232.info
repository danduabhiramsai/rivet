Name: BESIII_2023_I2637232
Year: 2023
Summary: $K^0_S$ momentum spectrum in $D\to K^0_SX$ decays
Experiment: BESIII
Collider: BEPC
InspireID: 2637232
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2302.14488 [hep-ex]
RunInfo: e+e- to D Dbat at psi(3770)
Beams: [e+, e-]
Energies: [3.773]
Description:
  'Measurement of $K^0_S$ momentum spectrum in $D\to K^0_SX$ decays. The uncorrected data was read from the figures in the paper and the background given subtracted.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2023has
BibTeX: '@article{BESIII:2023has,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Improved measurement of the branching fractions of the inclusive decays $D^+ \to K_S^0X $ and $D^0 \to K_S^0X $}",
    eprint = "2302.14488",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "2",
    year = "2023"
}
'
