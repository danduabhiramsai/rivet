Name: BESIII_2018_I1704558
Year: 2018
Summary: Cross section for $e^+e^-\to K^+K^-$ between 2.00 and 3.08 GeV
Experiment: BESIII
Collider: BEPC II
InspireID: 1704558
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:1811.08742
RunInfo: e+ e- to hadrons between 2.00 and 3.08 GeV
NeedCrossSection: yes
Beams: [e+, e-]
Energies: [2.0000, 2.0500, 2.1000, 2.1250, 2.1500, 2.1750, 2.2000, 2.2324, 2.3094,
           2.3864, 2.3960, 2.5000, 2.6444, 2.6464, 2.7000, 2.8000, 2.9000, 2.9500,
           2.9810, 3.0000, 3.0200, 3.0800]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to K^+K^-$ at energies between 2.00 and 3.08 GeV.
   Useful for comparing models of the kaon form factor.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
Keywords: []
BibKey: Ablikim:2018iyx
BibTeX: '@article{Ablikim:2018iyx,
      author         = "Ablikim, M. and others",
      title          = "{Measurement of $e^{+} e^{-} \rightarrow K^{+} K^{-}$
                        cross section at $\sqrt{s} = 2.00 - 3.08$ GeV}",
      collaboration  = "BESIII",
      year           = "2018",
      eprint         = "1811.08742",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      SLACcitation   = "%%CITATION = ARXIV:1811.08742;%%"
}'
