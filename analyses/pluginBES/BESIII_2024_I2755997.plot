BEGIN PLOT /BESIII_2024_I2755997/d01-x01-y01
Title=$\sigma(e^+e^-\to D^0\bar{D}^0)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to D^0\bar{D}^0)$/pb
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2024_I2755997/d02-x01-y01
Title=$\sigma(e^+e^-\to D^+D^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to D^+D^-)$/pb
ConnectGaps=1
END PLOT
