# BEGIN PLOT /OPAL_2007_I734955/d01-x01-y01
Title=
XLabel=$p_{\mathrm{T}}$~[GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_{\mathrm{T}}$ [pb/GeV]
YMin=2e-4
CustomLegend=$10<W<30$~GeV
LegendXPos=0.5
LegendYPos=0.95
RatioPlotSameStyle=1
RatioPlotYMin=0.0
RatioPlotYMax=1.6
# END PLOT

# BEGIN PLOT /OPAL_2007_I734955/d02-x01-y01
Title=
XLabel=$p_{\mathrm{T}}$~[GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_{\mathrm{T}}$ [pb/GeV]
YMin=2e-4
CustomLegend=$30<W<50$~GeV
LegendXPos=0.5
LegendYPos=0.95
RatioPlotSameStyle=1
RatioPlotYMin=0.0
RatioPlotYMax=1.6
# END PLOT

# BEGIN PLOT /OPAL_2007_I734955/d03-x01-y01
Title=
XLabel=$p_{\mathrm{T}}$~[GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_{\mathrm{T}}$ [pb/GeV]
YMin=2e-4
CustomLegend=$50<W<125$~GeV
LegendXPos=0.5
LegendYPos=0.95
RatioPlotSameStyle=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /OPAL_2007_I734955/d04-x01-y01
Title=
XLabel=$p_{\mathrm{T}}$~[GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_{\mathrm{T}}$ [pb/GeV]
YMin=2e-4
CustomLegend=$10<W<125$~GeV
LegendXPos=0.5
LegendYPos=0.95
RatioPlotSameStyle=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
LogX=0
# END PLOT

# BEGIN PLOT /OPAL_2007_I734955/d05-x01-y01
Title=
XLabel=$\eta$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta$ [pb/GeV]
YMax=6
YMin=0.01
LogY=0
CustomLegend=$10<W<30$~GeV
LegendXPos=0.5
LegendYPos=0.95
RatioPlotSameStyle=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /OPAL_2007_I734955/d06-x01-y01
Title=
XLabel=$\eta$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta$ [pb/GeV]
YMax=6
YMin=0.01
LogY=0
CustomLegend=$30<W<50$~GeV
LegendXPos=0.5
LegendYPos=0.95
RatioPlotSameStyle=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /OPAL_2007_I734955/d07-x01-y01
Title=
XLabel=$\eta$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta$ [pb/GeV]
YMax=6
YMin=0.01
LogY=0
CustomLegend=$50<W<125$~GeV
LegendXPos=0.5
LegendYPos=0.95
RatioPlotSameStyle=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /OPAL_2007_I734955/d08-x01-y01
Title=
XLabel=$\eta$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta$ [pb/GeV]
YMax=12
YMin=0.01
LogY=0
CustomLegend=$10<W<125$~GeV
LegendXPos=0.5
LegendYPos=0.95
RatioPlotSameStyle=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /OPAL_2007_I734955/d09-x01-y01
Title=
XLabel=$p_{\mathrm{T}}$~[GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_{\mathrm{T}}$ [pb/GeV]
YMin=2e-4
CustomLegend=$W>30$~GeV
LegendXPos=0.5
LegendYPos=0.95
RatioPlotSameStyle=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /OPAL_2007_I734955/d10-x01-y01
Title=
XLabel=$p_{\mathrm{T}}$~[GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_{\mathrm{T}}$ [pb/GeV]
YMin=2e-4
CustomLegend=$W>50$~GeV
LegendXPos=0.5
LegendYPos=0.95
RatioPlotSameStyle=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /OPAL_2007_I734955/d11-x01-y01
Title=
XLabel=$p_{\mathrm{T}}$~[GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_{\mathrm{T}}$ [pb/GeV]
YMin=2e-4
CustomLegend=$W>30$~GeV\\charged $\pi$
LegendXPos=0.5
LegendYPos=0.95
RatioPlotSameStyle=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /OPAL_2007_I734955/d12-x01-y01
Title=
XLabel=$p_{\mathrm{T}}$~[GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_{\mathrm{T}}$ [pb/GeV]
YMin=2e-4
CustomLegend=$W>50$~GeV\\charged $\pi$
LegendXPos=0.5
LegendYPos=0.95
RatioPlotSameStyle=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
LogX=0
# END PLOT
