BEGIN PLOT /DELPHI_1993_I356732/d01-x01-y01
Title=Scaled energy of $D^{*\pm}$ at $\sqrt{s}=91.2$~GeV
XLabel=$x_E$
YLabel=$1/N_\mathrm{Zhad} \mathrm{d}N(D^{*\pm})/\mathrm{d}x_E$
END PLOT
BEGIN PLOT /DELPHI_1993_I356732/d03-x01-y01
Title=Scaled energy of $D^0$ at $\sqrt{s}=91.2$~GeV
XLabel=$x_E$
YLabel=$1/N_\mathrm{Zhad} \mathrm{d}N(D^0)/\mathrm{d}x_E$
END PLOT
BEGIN PLOT /DELPHI_1993_I356732/d05-x01-y01
Title=Scaled energy of $D^+$ at $\sqrt{s}=91.2$~GeV
XLabel=$x_E$
YLabel=$1/N_\mathrm{Zhad} \mathrm{d}N(D^+)/\mathrm{d}x_E$
END PLOT