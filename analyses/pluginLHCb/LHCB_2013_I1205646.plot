BEGIN PLOT /LHCB_2013_I1205646/d01-x01-y01
Title=$J\/\psi$ transverse momentum $2.0 < |y| < 4.5$
XLabel=$p^{J\/\psi}_\perp$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
