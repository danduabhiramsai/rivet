Name: LHCB_2014_I1315113
Year: 2014
Summary: Relative production rates of $\chi_{b1}(1P)$ to $\chi_{b2}(1P)$ at 7 TeV
Experiment: LHCB
Collider: LHC
InspireID: 1315113
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 10 (2014) 088
RunInfo: chi_b(1,2) production
Beams: [p+, p+]
Energies: [7000]
Description:
  'Measurement of the relative rates of prompt  $\chi_{b1}$ to  $\chi_{b2}$ production at 7 TeV using the decay to $\Upsilon\gamma$ by the LHCb collaboration.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2014nug
BibTeX: '@article{LHCb:2014nug,
    author = "Aaij, Roel and others",
    collaboration = "LHCb",
    title = "{Measurement of the $\chi_b(3P)$ mass and of the relative rate of $\chi_{b1}(1P)$ and $\chi_{b2}(1P)$ production}",
    eprint = "1409.1408",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "LHCB-PAPER-2014-040, CERN-PH-EP-2014-206, LHCB-PAPER-2014-040-CERN\_PH\_EP\_2014\_206",
    doi = "10.1007/JHEP10(2014)088",
    journal = "JHEP",
    volume = "10",
    pages = "088",
    year = "2014"
}
'
