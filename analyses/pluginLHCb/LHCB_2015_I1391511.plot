BEGIN PLOT /LHCB_2015_I1391511
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d03
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction
END PLOT

BEGIN PLOT /LHCB_2015_I1391511/d01-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d01-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d01-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d01-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d01-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2015_I1391511/d02-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d02-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d02-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d02-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d02-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2015_I1391511/d03-x01-y01
Title=Non-prompt J/$\psi$ fraction ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d03-x01-y02
Title=Non-prompt J/$\psi$ fraction ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d03-x01-y03
Title=Non-prompt J/$\psi$ fraction ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d03-x01-y04
Title=Non-prompt J/$\psi$ fraction ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d03-x01-y05
Title=Non-prompt J/$\psi$ fraction ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2015_I1391511/d04-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for prompt J/$\psi$ ($2.0<y<4.5$) 
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d05-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for non-prompt J/$\psi$ ($2.0<y<4.5$) 
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT

BEGIN PLOT /LHCB_2015_I1391511/d06-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for prompt J/$\psi$ ($2.0<y<4.5$) 
XLabel=$y$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [$\mu$b]
END PLOT
BEGIN PLOT /LHCB_2015_I1391511/d07-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for non-prompt J/$\psi$ ($2.0<y<4.5$) 
XLabel=$y$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [$\mu$b]
END PLOT
