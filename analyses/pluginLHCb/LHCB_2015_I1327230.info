Name: LHCB_2015_I1327230
Year: 2015
Summary: $B_c^+$ meson production at 8 TeV
Experiment: LHCB
Collider: LHC
InspireID: 1327230
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 114 (2015) 132001
RunInfo: hadronic events with B meson
Beams: [p+, p+]
Energies: [8000]
Options :
 - BPBR=*
 - BCBR=*
Description:
  'Measurement of the double differential (in $p_\perp$ and $y$) ratio $B_c^+/B^+$ meson production at 8 TeV by the LHCb collaboration. The
   branching ratio for the decay $B^+\to J\psi K^+$ is taken to be $0.00102$ from PDG2023 while by default the branching ratio for $B_c^+\to J\psi\pi^+$ is
   measured from the events, however it can be specified using the BCBR option.'
ValidationInfo:
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2014mvo
BibTeX: '@article{LHCb:2014mvo,
    author = "Aaij, Roel and others",
    collaboration = "LHCb",
    title = "{Measurement of $B_c^+$ production in proton-proton collisions at $\sqrt{s}=8$ TeV}",
    eprint = "1411.2943",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CERN-PH-EP-2014-269, LHCB-PAPER-2014-050",
    doi = "10.1103/PhysRevLett.114.132001",
    journal = "Phys. Rev. Lett.",
    volume = "114",
    pages = "132001",
    year = "2015"
}
'
