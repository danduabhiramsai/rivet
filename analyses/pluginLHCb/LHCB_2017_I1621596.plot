BEGIN PLOT /LHCB_2017_I1621596/
YMin=-1.
YMax=1.
LogY=0
XLabel=$p_\perp$ [GeV]
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d01-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(1S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d01-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d01-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d02-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(1S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d02-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d02-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d03-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(1S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d03-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d03-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d04-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(1S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d04-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d04-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d05-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(1S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d05-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d05-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d06-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(1S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d06-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d06-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d07-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(1S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d07-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d07-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d08-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(1S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d08-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d08-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d09-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(1S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d09-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d09-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d10-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(1S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d10-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d10-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d11-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(1S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d11-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/11-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d12-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(1S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d12-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d12-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d13-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(1S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d13-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d13-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d14-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(1S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d14-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d14-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d15-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(1S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d15-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d15-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d16-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(1S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d16-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d16-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT


BEGIN PLOT /LHCB_2017_I1621596/d17-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(1S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d17-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d17-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d18-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(1S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d18-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d18-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d19-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(1S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d19-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d19-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d20-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(1S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d20-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d20-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d21-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(1S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d21-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d21-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d22-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(1S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d22-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d22-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d23-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(1S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d23-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d23-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d24-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(1S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d24-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d24-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d25-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(1S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d25-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(1S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d25-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(1S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d26-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(1S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d26-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(1S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d26-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(1S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d27-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(1S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d27-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(1S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d27-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(1S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d28-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(1S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d28-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(1S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d28-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(1S)$ at 7 TeV
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d29-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(1S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d29-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(1S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d29-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(1S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d30-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(1S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d30-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(1S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d30-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(1S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d31-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(1S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d31-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(1S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d31-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(1S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d32-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(1S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d32-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(1S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d32-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(1S)$ at 8 TeV
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d33-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(2S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d33-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d33-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d34-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(2S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d34-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d34-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d35-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(2S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d35-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d35-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d36-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(2S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d36-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d36-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d37-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(2S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d37-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d37-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d38-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(2S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d38-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d38-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d39-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(2S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d39-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d39-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d40-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(2S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d40-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d40-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d41-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(2S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d41-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d41-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d42-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(2S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d42-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d42-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d43-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(2S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d43-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d43-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d44-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(2S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d44-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d44-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d45-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(2S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d45-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d45-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d46-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(2S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d46-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d46-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d47-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(2S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d47-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d47-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d48-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(2S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d48-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d48-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT


BEGIN PLOT /LHCB_2017_I1621596/d49-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(2S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d49-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d49-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d50-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(2S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d50-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d50-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d51-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(2S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d51-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d51-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d52-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(2S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d52-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d52-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d53-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(2S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d53-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d53-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d54-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(2S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d54-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d54-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d55-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(2S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d55-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d55-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d56-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(2S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d56-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d56-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d57-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(2S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d57-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(2S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d57-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(2S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d58-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(2S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d58-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(2S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d58-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(2S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d59-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(2S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d59-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(2S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d59-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(2S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d60-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(2S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d60-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(2S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d60-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(2S)$ at 7 TeV
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d61-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(2S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d61-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(2S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d61-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(2S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d62-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(2S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d62-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(2S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d62-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(2S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d63-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(2S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d63-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(2S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d63-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(2S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d64-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(2S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d64-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(2S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d64-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(2S)$ at 8 TeV
END PLOT


BEGIN PLOT /LHCB_2017_I1621596/d65-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(3S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d65-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(3S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d65-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(3S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d66-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(3S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d66-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(3S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d66-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(3S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d67-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(3S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d67-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(3S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d67-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(3S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d68-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(3S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d68-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(3S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d68-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(3S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d69-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(3S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d69-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d69-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d70-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(3S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d70-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d70-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d71-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(3S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d71-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d71-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d72-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(3S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d72-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d72-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d73-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(3S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d73-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(3S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d73-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(3S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d74-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(3S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d74-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(3S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d74-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(3S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d75-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(3S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d75-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(3S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d75-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(3S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d76-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(3S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d76-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(3S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d76-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(3S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d77-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(3S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d77-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d77-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d78-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(3S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d78-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d78-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d79-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(3S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d79-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d79-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d80-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(3S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d80-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d80-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT


BEGIN PLOT /LHCB_2017_I1621596/d81-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(3S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d81-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(3S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d81-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(3S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d82-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(3S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d82-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(3S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d82-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(3S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d83-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(3S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d83-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(3S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d83-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(3S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d84-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(3S)$ at 7 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d84-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(3S)$ at 7 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d84-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(3S)$ at 7 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d85-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(3S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d85-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d85-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d86-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(3S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d86-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d86-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d87-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(3S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d87-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d87-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d88-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(3S)$ at 8 TeV ($2.2<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d88-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d88-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d89-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(3S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d89-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(3S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d89-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(3S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d90-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(3S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d90-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(3S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d90-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(3S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d91-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(3S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d91-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(3S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d91-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(3S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d92-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(3S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d92-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(3S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d92-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(3S)$ at 7 TeV
END PLOT

BEGIN PLOT /LHCB_2017_I1621596/d93-x01-y01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(3S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d93-x01-y02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(3S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d93-x01-y03
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in GJ frame for $\Upsilon(3S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d94-x01-y01
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(3S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d94-x01-y02
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(3S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d94-x01-y03
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in GJ frame for $\Upsilon(3S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d95-x01-y01
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(3S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d95-x01-y02
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(3S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d95-x01-y03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in GJ frame for $\Upsilon(3S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d96-x01-y01
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(3S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d96-x01-y02
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(3S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2017_I1621596/d96-x01-y03
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in GJ frame for $\Upsilon(3S)$ at 8 TeV
END PLOT
