BEGIN PLOT /LHCB_2012_I1107645/d01-x01-y01
Title=Fraction of prompt J/$\psi$ from $\chi_{c(1,2)}\to\gamma J/\psi$ as function of J/$\psi$ $p_\perp$
XLabel=$p_\perp^{J/\psi}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{J/\psi}$
END PLOT
