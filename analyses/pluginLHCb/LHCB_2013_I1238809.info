Name: LHCB_2013_I1238809
Year: 2013
Summary: B meson production at 7 TeV
Experiment: LHCB
Collider: LHC
InspireID: 1238809
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 08 (2013) 117
RunInfo: hadronic events with B meson
Beams: [p+, p+]
Energies: [7000]
Description:
  'Measurement of the double differential (in $p_\perp$ and $y$) cross section for B meson production at 7 TeV by the LHCb collaboration.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2013vjr
BibTeX: '@article{LHCb:2013vjr,
    author = "Aaij, R and others",
    collaboration = "LHCb",
    title = "{Measurement of B meson production cross-sections in proton-proton collisions at $\sqrt{s}$ = 7 TeV}",
    eprint = "1306.3663",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CERN-PH-EP-2013-095, LHCB-PAPER-2013-004",
    doi = "10.1007/JHEP08(2013)117",
    journal = "JHEP",
    volume = "08",
    pages = "117",
    year = "2013"
}'
