BEGIN PLOT /ZEUS_2008_I780108/d11-x01-y01
Title=Unpolarized inclusive jet cross section vs the jet $\eta$, $e^-$
YLabel=$\text{d}\sigma/\text{d}\eta$ [pb]
XLabel=$\eta_\text{jet}$
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d11-x01-y02
Title=Unpolarized inclusive jet cross section vs jet $\eta$, $e^+$
YLabel=$\text{d}\sigma/\text{d}\eta$ [pb]
XLabel=$\eta_\text{jet}$
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d12-x01-y01
Title=Unpolarized inclusive two jet cross section vs jet $\eta$, $e^-$
YLabel=$\text{d}\sigma/\text{d}\eta$ [pb]
XLabel=$\eta_\text{jet}$
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d12-x01-y02
Title=Unpolarized inclusive two jet cross section vs jet $\eta$, $e^+$
YLabel=$\text{d}\sigma/\text{d}\eta$ [pb]
XLabel=$\eta_\text{jet}$
END PLOT


BEGIN PLOT /ZEUS_2008_I780108/d13-x01-y01
Title=Unpolarized inclusive three jet cross section vs jet $\eta$, $e^-$
YLabel=$\text{d}\sigma/\text{d}\eta$ [pb]
XLabel=$\eta_\text{jet}$
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d13-x01-y02
Title=Unpolarized inclusive three jet cross section vs jet $\eta$, $e^+$
YLabel=$\text{d}\sigma/\text{d}\eta$ [pb]
XLabel=$\eta_\text{jet}$
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d14-x01-y01
Title=Unpolarized inclusive jet cross section vs jet $E_\perp$, $e^-$
YLabel=$\text{d}\sigma/\text{d}E_\perp$ [$\text{pb}/\text{GeV}$]
XLabel=$E_\perp$ [GeV]
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d14-x01-y02
Title=Unpolarized inclusive jet cross section vs jet $E_\perp$, $e^+$
YLabel=$\text{d}\sigma/\text{d}E_\perp$ [$\text{pb}/\text{GeV}$]
XLabel=$E_\perp$ [GeV]
END PLOT


BEGIN PLOT /ZEUS_2008_I780108/d15-x01-y01
Title=Unpolarized inclusive two jet cross section vs $E_\perp$, $e^-$
YLabel=$\text{d}\sigma/\text{d}E_\perp$ [$\text{pb}/\text{GeV}$]
XLabel=$E_\perp$ [GeV]
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d15-x01-y02
Title=Unpolarized inclusive two jet cross section vs $E_\perp$, $e^+$
YLabel=$\text{d}\sigma/\text{d}E_\perp$ [$\text{pb}/\text{GeV}$]
XLabel=$E_\perp$ [GeV]
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d16-x01-y01
Title=Unpolarized inclusive three jet cross section vs $E_\perp$, $e^-$
YLabel=$\text{d}\sigma/\text{d}E_\perp$ [$\text{pb}/\text{GeV}$]
XLabel=$E_\perp$ [GeV]
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d16-x01-y02
Title=Unpolarized inclusive three jet cross section vs $E_\perp$, $e^+$
YLabel=$\text{d}\sigma/\text{d}E_\perp$ [$\text{pb}/\text{GeV}$]
XLabel=$E_\perp$ [GeV]
END PLOT


BEGIN PLOT /ZEUS_2008_I780108/d17-x01-y01
LogX=1
Title=Unpolarized inclusive jet cross section vs $Q^2$, $e^-$
YLabel=$\text{d}\sigma/\text{d}Q^2$ [$\text{pb}/\text{GeV}^{-2}$]
XLabel=$Q^2$ [$\text{GeV}^2$]
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d17-x01-y02
LogX=1
Title=Unpolarized inclusive jet cross section vs $Q^2$, $e^+$
YLabel=$\text{d}\sigma/\text{d}Q^2$ [$\text{pb}\text{GeV}^{-2}$]
XLabel=$Q^2$ [$\text{GeV}^2$]
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d18-x01-y01
LogX=1
Title=Unpolarized inclusive two jet cross section vs $Q^2$, $e^-$
YLabel=$\text{d}\sigma/\text{d}Q^2$ [$\text{pb}\text{GeV}^{-2}$]
XLabel=$Q^2$ [$\text{GeV}^2$]
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d18-x01-y02
LogX=1
Title=Unpolarized inclusive two jet cross section vs $Q^2$, $e^+$
YLabel=$\text{d}\sigma/\text{d}Q^2$ [$\text{pb}\text{GeV}^{-2}$]
XLabel=$Q^2$ [$\text{GeV}^2$]
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d19-x01-y01
LogX=1
Title=Unpolarized inclusive three jet cross section vs $Q^2$, $e^-$
YLabel=$\text{d}\sigma/\text{d}Q^2$ [$\text{pb}\text{GeV}^{-2}$]
XLabel=$Q^2$ [$\text{GeV}^2$]
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d19-x01-y02
LogX=1
Title=Unpolarized inclusive three jet cross section vs $Q^2$, $e^+$
YLabel=$\text{d}\sigma/\text{d}Q^2$ [$\text{pb}\text{GeV}^{-2}$]
XLabel=$Q^2$ [$\text{GeV}^2$]
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d20-x01-y01
LogX=1
Title=Unpolarized inclusive jet cross section vs $x$, $e^-$
XLabel=$log_{10}(x)$
YLabel=$\text{d}\sigma/\text{d}log_{10}(x)$ [pb]
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d20-x01-y02
LogX=1
Title=Unpolarized inclusive jet cross section vs $x$, $e^+$
XLabel=$log_{10}(x)$
YLabel=$\text{d}\sigma/\text{d}log_{10}(x)$ [pb]
END PLOT



BEGIN PLOT /ZEUS_2008_I780108/d21-x01-y01
LogX=1
Title=Integrated unpolarized inclusive jet cross section in the given kinemetical region, E- P --> NUE JET X
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d21-x01-y02
LogX=1
Title=Integrated unpolarized inclusive jet cross section in the given kinemetical region, E+ P --> NUEBAR JET X
END PLOT


BEGIN PLOT /ZEUS_2008_I780108/d21-x01-y03
LogX=1
Title=Integrated unpolarized dijet cross section in the given kinemetical region, E- P --> NUE JET JET X
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d21-x01-y04
LogX=1
Title=Integrated unpolarized dijet cross section in the given kinemetical region, E+ P --> NUEBAR JET JET X
END PLOT


BEGIN PLOT /ZEUS_2008_I780108/d21-x01-y05
LogX=1
Title=Integrated unpolarized trijet cross section in the given kinemetical region, E- P --> NUE JET JET JET X
XLabel=$x$
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d21-x01-y06
LogX=1
Title=Integrated unpolarized trijet cross section in the given kinemetical region, E+ P --> NUEBAR JET JET JET X
XLabel=$x$
END PLOT


BEGIN PLOT /ZEUS_2008_I780108/d22-x01-y01
Title=Unpolarized dijet cross section vs dijet mass.
XLabel=$M^{jj}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}M^{jj}$ [$\text{pb}/\text{GeV}$]
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d22-x01-y02
Title=Unpolarized dijet cross section vs dijet mass.
XLabel=$M^{jj}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}M^{jj}$ [$\text{pb}/\text{GeV}$]
END PLOT
,


BEGIN PLOT /ZEUS_2008_I780108/d23-x01-y01
Title=Unpolarized inclusive three jet cross section vs trijet mass.
XLabel=$M^{jjj}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}M^{jjj}$ [$\text{pb}/\text{GeV}$]
END PLOT

BEGIN PLOT /ZEUS_2008_I780108/d23-x01-y02
Title=Unpolarized inclusive three jet cross section vs trijet mass.
XLabel=$M^{jjj}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}M^{jjj}$ [$\text{pb}/\text{GeV}$]
END PLOT


