# BEGIN PLOT /H1_2006_I711847/d01-x01-y01
XLabel=$|\cos \theta^*|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|\cos \theta^*|$ [pb]
RatioPlotSameStyle=1
LogY=0
CustomLegend=$x_{\gamma} < 0.8$
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d01-x01-y02
XLabel=$|\cos \theta^*|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|\cos \theta^*|$ [pb]
RatioPlotSameStyle=1
LogY=0
CustomLegend=$x_{\gamma} > 0.8$
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d02-x01-y01
XLabel=$|\cos \theta^*|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|\cos \theta^*|$ [pb]
RatioPlotSameStyle=1
LogY=0
CustomLegend=$x_{\gamma} < 0.8$\\$M_{\rm jj} > 65$ GeV
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d02-x01-y02
XLabel=$|\cos \theta^*|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|\cos \theta^*|$ [pb]
RatioPlotSameStyle=1
LogY=0
CustomLegend=$x_{\gamma} > 0.8$\\$M_{\rm jj} > 65$ GeV
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d03-x01-y01
XLabel=$x_{\gamma}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}x_{\gamma}$ [pb]
RatioPlotSameStyle=1
LogY=0
CustomLegend=$x_{\rm p} < 0.1$
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d03-x01-y02
XLabel=$x_{\gamma}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}x_{\gamma}$ [pb]
RatioPlotSameStyle=1
LogY=0
CustomLegend=$x_{\rm p} > 0.1$
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d04-x01-y01
XLabel=$x_{\rm p}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}x_{\rm p}$ [pb]
RatioPlotSameStyle=1
CustomLegend=$x_{\gamma} < 0.8$\\$\eta_{1,2} < 1$
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d05-x01-y01
XLabel=$x_{\rm p}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}x_{\rm p}$ [pb]
RatioPlotSameStyle=1
CustomLegend=$x_{\gamma} < 0.8$\\$\eta_i < 1,\,\eta_j > 1 $
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d06-x01-y01
XLabel=$x_{\rm p}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}x_{\rm p}$ [pb]
RatioPlotSameStyle=1
CustomLegend=$x_{\gamma} < 0.8$\\$\eta_{1,2} > 1 $
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d07-x01-y01
XLabel=$x_{\rm p}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}x_{\rm p}$ [pb]
RatioPlotSameStyle=1
CustomLegend=$x_{\gamma} > 0.8$\\$\eta_{1,2} < 1$
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d08-x01-y01
XLabel=$x_{\rm p}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}x_{\rm p}$ [pb]
RatioPlotSameStyle=1
CustomLegend=$x_{\gamma} > 0.8$\\$\eta_i < 1,\,\eta_j > 1 $
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d09-x01-y01
XLabel=$x_{\rm p}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}x_{\rm p}$ [pb]
RatioPlotSameStyle=1
CustomLegend=$x_{\gamma} > 0.8$\\$\eta_{1,2} > 1 $
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d10-x01-y01
XLabel=$E_{\rm t,max}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\rm t,max}$ [pb/GeV]
RatioPlotSameStyle=1
CustomLegend=$x_{\gamma} < 0.8$\\$\eta_{1,2} < 1$
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d11-x01-y01
XLabel=$E_{\rm t, max}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\rm t,max}$ [pb/GeV]
RatioPlotSameStyle=1
CustomLegend=$x_{\gamma} < 0.8$\\$\eta_i < 1,\,\eta_j > 1 $
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d12-x01-y01
XLabel=$E_{\rm t, max}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\rm t,max}$ [pb/GeV]
RatioPlotSameStyle=1
CustomLegend=$x_{\gamma} < 0.8$\\$\eta_{1,2} > 1 $
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d13-x01-y01
XLabel=$E_{\rm t, max}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\rm t,max}$ [pb/GeV]
RatioPlotSameStyle=1
CustomLegend=$x_{\gamma} > 0.8$\\$\eta_{1,2} < 1$
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d14-x01-y01
XLabel=$E_{\rm t, max}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\rm t,max}$ [pb/GeV]
RatioPlotSameStyle=1
CustomLegend=$x_{\gamma} > 0.8$\\$\eta_i < 1,\,\eta_j > 1 $
# END PLOT

# BEGIN PLOT /H1_2006_I711847/d15-x01-y01
XLabel=$E_{\rm t, max}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\rm t,max}$ [pb/GeV]
RatioPlotSameStyle=1
CustomLegend=$x_{\gamma} > 0.8$\\$\eta_{1,2} > 1 $
# END PLOT
