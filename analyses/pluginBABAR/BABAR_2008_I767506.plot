BEGIN PLOT /BABAR_2008_I767506/d01-x01-y01
Title=$J/\psi\omega$ mass in $B^+\to J/\psi\omega K^-$
XLabel=$m_{J/\psi\omega}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\omega}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I767506/d01-x01-y02
Title=$J/\psi\omega$ mass in $B^0\to J/\psi\omega K^0$
XLabel=$m_{J/\psi\omega}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\omega}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
