BEGIN PLOT /BABAR_2012_I1122034/
LogY=0
END PLOT

BEGIN PLOT /BABAR_2012_I1122034/d01-x01-y01
Title=$J/\psi\omega$ mass distribution in $e^+e^-\to e^+e^- J/\psi\omega$
XLabel=$m_{J/\psi\omega}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{J/\psi\omega}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BABAR_2012_I1122034/d02-x01-y01
Title=$\cos\theta^*_\ell$ distribution in $e^+e^-\to e^+e^- J/\psi\omega$
XLabel=$\cos\theta^*_\ell$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta^*_\ell$
END PLOT
BEGIN PLOT /BABAR_2012_I1122034/d02-x01-y02
Title=$\cos\theta^*_n$ distribution in $e^+e^-\to e^+e^- J/\psi\omega$
XLabel=$\cos\theta^*_n$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta^*_n$
END PLOT
BEGIN PLOT /BABAR_2012_I1122034/d02-x01-y03
Title=$\cos\theta_{\ell n}$ distribution in $e^+e^-\to e^+e^- J/\psi\omega$
XLabel=$\cos\theta_{\ell n}$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_{\ell n}$
END PLOT
BEGIN PLOT /BABAR_2012_I1122034/d02-x01-y04
Title=$\cos\theta_h$ distribution in $e^+e^-\to e^+e^- J/\psi\omega$
XLabel=$\cos\theta_h$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_h$
END PLOT

BEGIN PLOT /BABAR_2012_I1122034/d03-x01-y01
Title=$\cos\theta_n$ distribution in $e^+e^-\to e^+e^- J/\psi\omega$
XLabel=$\cos\theta_n$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_n$
END PLOT
BEGIN PLOT /BABAR_2012_I1122034/d03-x01-y02
Title=$\cos\theta_\ell$ distribution in $e^+e^-\to e^+e^- J/\psi\omega$
XLabel=$\cos\theta_\ell$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_\ell$
END PLOT
BEGIN PLOT /BABAR_2012_I1122034/d03-x01-y03
Title=$\phi_\ell$ distribution in $e^+e^-\to e^+e^- J/\psi\omega$
XLabel=$\phi_\ell$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\phi_\ell$
END PLOT
