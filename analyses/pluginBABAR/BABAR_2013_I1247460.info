Name: BABAR_2013_I1247460
Year: 2013
Summary: Semi-leptonic $B^+\to\omega\ell^+\nu_\ell$
Experiment: BABAR
Collider: PEP-II
InspireID: 1247460
Status: VALIDATED NOHEPDATA 
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 88 (2013) 7, 072006
RunInfo: Any process producing B+ -> omega l+ nu_l, original e+e->Upsilon(4S)
Description:
  'Implementation of Lorentz invariant $q^2$ distributions ("form factor") for semileptonic $B^+$ decays, uses the same dataset as BABAR_2013_I1116411 but different technique to extract the semi-leptonic decays.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BaBar:2013pls
BibTeX: '@article{BaBar:2013pls,
    author = "Lees, J. P. and others",
    collaboration = "BaBar",
    title = "{Measurement of the $B^+ \to \omega \ell^+ \nu$ branching fraction with semileptonically tagged B mesons}",
    eprint = "1308.2589",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "BABAR-PUB-13-007, SLAC-PUB-15644",
    doi = "10.1103/PhysRevD.88.072006",
    journal = "Phys. Rev. D",
    volume = "88",
    number = "7",
    pages = "072006",
    year = "2013"
}
'
