BEGIN PLOT /BABAR_2009_I785439/d01-x01-y01
Title=$K^+\eta$ mass  distribution in $B^+\to K^+\eta\gamma$
XLabel=$m_{K^+\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I785439/d01-x01-y02
Title=$K^0\eta$ mass  distribution in $B^0\to K^0\eta\gamma$
XLabel=$m_{K^0\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
