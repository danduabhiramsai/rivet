BEGIN PLOT /BABAR_2012_I1081760/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $B^0\to\pi^+\pi^- K^{*0}$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
