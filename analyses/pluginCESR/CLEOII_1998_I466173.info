Name: CLEOII_1998_I466173
Year: 1998
Summary: Helicity angle in $D_s^+\to\rho^+\eta^\prime$
Experiment: CLEOII
Collider: CESR
InspireID: 466173
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 58 (1998) 052002
RunInfo: Any process producing D_s+, originally e+ e-
Description:
  'Measurement of the helicity angle in $D_s^+\to\rho^+\eta^\prime$. The data were read from figure 9 in the paper.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CLEO:1998dbn
BibTeX: '@article{CLEO:1998dbn,
    author = "Jessop, C. P. and others",
    collaboration = "CLEO",
    title = "{Measurement of the branching ratios for the decays of D+(s) to eta pi+, eta-prime pi+, eta rho+, and eta-prime rho+}",
    eprint = "hep-ex/9801010",
    archivePrefix = "arXiv",
    reportNumber = "SLAC-REPRINT-1998-084, CLNS-97-1515, CLEO-97-22",
    doi = "10.1103/PhysRevD.58.052002",
    journal = "Phys. Rev. D",
    volume = "58",
    pages = "052002",
    year = "1998"
}
'
