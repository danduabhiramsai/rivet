BEGIN PLOT /CLEO_1997_I443704/d01-x01-y01
Title=$B\to D\ell^+\nu_\ell$
XLabel=$w$
YLabel=$\text{d}\Gamma/\text{d}w$ [$\text{ns}^{1-}$]
LogY=0
END PLOT
