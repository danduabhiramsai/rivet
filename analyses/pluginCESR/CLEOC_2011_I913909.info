Name: CLEOC_2011_I913909
Year: 2011
Summary:  Dalitz plot analysis of $D^0\to K_S^0\pi^0\pi^0$
Experiment: CLEOC
Collider: CESR
InspireID: 913909
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 84 (2011) 092005
RunInfo: Any process producing D0 -> KS0 pi0 pi0
Description:
  'Measurement of the mass distributions in the decay $D^0\to K_S^0\pi^0\pi^0$. The data were read from the plots in the paper, with the background given subtracted.
   Resolution/acceptance effects have been not unfolded and given the agreement with the model in the paper this analysis should only be used for qualitative studies.'
ValidationInfo:
  'Herwig 7 events using model from paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CLEO:2011cnt
BibTeX: '@article{CLEO:2011cnt,
    author = "Lowrey, N. and others",
    collaboration = "CLEO",
    title = "{Analysis of the Decay $D^0 \to K^0_S \pi^0 \pi^0$}",
    eprint = "1106.3103",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CLNS-10-2069, CLEO-10-06",
    doi = "10.1103/PhysRevD.84.092005",
    journal = "Phys. Rev. D",
    volume = "84",
    pages = "092005",
    year = "2011"
}
'
