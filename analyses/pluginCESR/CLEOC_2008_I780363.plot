BEGIN PLOT /CLEOC_2008_I780363/d01-x01-y01
Title=$K^-\pi^+$ mass distribution
XLabel=$m_{K^-\pi^+}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOC_2008_I780363/d01-x02-y01
Title=$\pi^+\pi^+$ mass distribution
XLabel=$m_{\pi^+\pi^+}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
