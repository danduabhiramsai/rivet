BEGIN PLOT /CLEOII_1999_I501487/d01-x01-y01
Title=$\pi^+\pi^0$ mass in $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau(\text{non}-\omega)$
XLabel=$m_{\pi^+\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1999_I501487/d01-x01-y02
Title=$\pi^-\pi^0$ mass in $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau(\text{non}-\omega)$
XLabel=$m_{\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1999_I501487/d01-x01-y03
Title=$\pi^+\pi^-$ mass in $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau(\text{non}-\omega)$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /CLEOII_1999_I501487/d02-x01-y01
Title=Hadronic mass in $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau(\omega)$
XLabel=$m_{4\pi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{4\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1999_I501487/d02-x01-y02
Title=Hadronic mass in $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau(\text{non}-\omega)$
XLabel=$m_{4\pi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{4\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
