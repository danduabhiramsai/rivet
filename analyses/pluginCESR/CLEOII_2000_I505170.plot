BEGIN PLOT /CLEOII_2000_I505170/
LogY=0
END PLOT

BEGIN PLOT /CLEOII_2000_I505170/d01-x01-y01
Title=Spectral function for $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau$
XLabel=$q$ [GeV]
YLabel=$v_1(q)$
END PLOT
BEGIN PLOT /CLEOII_2000_I505170/d01-x01-y02
Title=Spectral function for $\tau\to\omega\pi^-\nu_\tau$
XLabel=$q$ [GeV]
YLabel=$v_1(q)$
END PLOT
BEGIN PLOT /CLEOII_2000_I505170/d01-x01-y03
Title=Spectral function for $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau$ (non-$\omega$)
XLabel=$q$ [GeV]
YLabel=$v_1(q)$
END PLOT

BEGIN PLOT /CLEOII_2000_I505170/d02-x01-y01
Title=Helicity angle for $\tau\to\omega\pi^-\nu_\tau$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\cos\theta$
END PLOT

BEGIN PLOT /CLEOII_2000_I505170/d03-x01-y01
Title=$\pi^+\pi^-\pi^0$ mass in $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau$
XLabel=$m_{\pi^+\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /CLEOII_2000_I505170/d03-x01-y02
Title=$\pi^-\pi^0$ mass in $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau$
XLabel=$m_{\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /CLEOII_2000_I505170/d03-x01-y03
Title=$\pi^+\pi^0$ mass in $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau$
XLabel=$m_{\pi^+\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /CLEOII_2000_I505170/d03-x01-y04
Title=$\pi^+\pi^-$ mass in $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /CLEOII_2000_I505170/d03-x01-y05
Title=$2\pi^-\pi^+\pi^0$ mass in $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau$
XLabel=$m_{2\pi^-\pi^+\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{2\pi^-\pi^+\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /CLEOII_2000_I505170/d03-x01-y06
Title=$2\pi^-\pi^+$ mass in $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau$
XLabel=$m_{2\pi^-\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{2\pi^-\pi^+}$ [$\text{GeV}^{-1}$]
END PLOT

