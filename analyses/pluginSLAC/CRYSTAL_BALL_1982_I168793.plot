BEGIN PLOT /CRYSTAL_BALL_1982_I168793/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^0\pi^0$ with $|\cos\theta|<0.7$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \pi^0\pi^0)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1982_I168793/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^0\pi^0$ with $1.04<\sqrt{s}<1.48$ GeV
LogY=0
YLabel=$\text{d}\sigma/\text{d}|\cos\theta|$ [nb]
XLabel=$|\cos\theta|$
END PLOT
