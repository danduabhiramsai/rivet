BEGIN PLOT /MARKII_1982_I177606/d02-x01-y01
Title=$D^{*\pm}$ spectrum at 29 GeV
XLabel=$x_E$
YLabel=$\frac{s}{\beta} \mathrm{d}\sigma/\mathrm{d}x_E$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /MARKII_1982_I177606/d03-x01-y01
Title=$D^{*\pm}$ spectrum at 29 GeV
XLabel=$x_E$
YLabel=$\frac{s}{\beta} \mathrm{d}\sigma/\mathrm{d}x_E$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
END PLOT
