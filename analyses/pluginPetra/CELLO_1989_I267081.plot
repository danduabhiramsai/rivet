BEGIN PLOT /CELLO_1989_I267081/d0[1,2]-x01-y01
Title=Cross section for $\gamma\gamma\to \rho^+\rho^-$
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
