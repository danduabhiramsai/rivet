BEGIN PLOT /CRYSTAL_BALL_1986_I217547/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $|\cos\theta|<0.8$
XLabel=$\sqrt{s}$ [MeV]
YLabel=$\sigma(\gamma\gamma\to \eta\pi^0)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1986_I217547/d02-x01-y01
LogY=0
YLabel=$\text{d}\sigma/\text{d}|\cos\theta|$ [nb]
XLabel=$|\cos\theta|$
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $0.9<\sqrt{s}<1.1$ GeV
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1986_I217547/d02-x01-y02
LogY=0
YLabel=$\text{d}\sigma/\text{d}|\cos\theta|$ [nb]
XLabel=$|\cos\theta|$
Title=Cross section for $\gamma\gamma\to \eta\pi^0$ with $1.1<\sqrt{s}<1.48$ GeV
END PLOT