BEGIN PLOT /ARGUS_1994_I372451/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \rho\phi$
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1994_I372451/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \omega\phi$
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
