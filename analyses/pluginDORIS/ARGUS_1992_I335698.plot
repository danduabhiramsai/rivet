BEGIN PLOT /ARGUS_1992_I335698/d01-x01-y01
Title=$\pi^-\pi^0$ mass in $\tau\to\pi^-\pi^0\nu_\tau$
XLabel=$m_{\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1992_I335698/d02-x01-y01
Title=$\pi^-\pi^0$ helicity angle in $\tau\to\pi^-\pi^0\nu_\tau$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
LegendYPos=0.2
END PLOT
